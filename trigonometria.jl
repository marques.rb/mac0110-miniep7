#MAC0110 - MiniEP7
# Beatriz Marques - 11932334


#Fatorial
function fatorial(f)
 if f <= 1
  return 1
 else
  return f*fatorial(f-1)
 end
end 

#Potência
function potencia(b, ex)
res = 1
  while ex > 0
   res = res * b
   ex = ex - 1
  end
return res
end

# Número de Bernoulli
function bernoulli(n)
n *= 2
A = Vector{Rational{BigInt}}(undef, n + 1)
 for m = 0 : n
  A[m + 1] = 1 // (m + 1)
 for j = m : -1 : 1
  A[j] = j * (A[j] - A[j + 1])
 end
 end
return abs(A[1])
end

#Teste Cosseno
function check_cos(value,x)
print("Valor de Cosseno:")
value = convert(Float32,big(value))
x = convert(Float32,big(x))
println(x)
 if x == value 
  return true
 else
return false
 end
end

#Teste Seno
function check_sin(value,x)
print("Valor de Seno:")
value = convert(Float32,big(value))
x = convert(Float32,big(x))
println(x)
 if x == value
  return true
 else
  return false
end
end

#Teste Tangente
function check_tan(value, x)
print("Valor de Tangente:")
value = BigFloat(value, 10)
x = BigFloat(x, 10)
println(x)
 if x == value 
  return true
 else
  return false
end
end 

<<<<<<< HEAD
#Cosseno
function taylor_cos(x)
Erro = 1e-8
termo = 1
soma = 1
ex = 1
 while abs(termo) > Erro
  termo = -1 * termo * x * x /((2*ex)*(2*ex-1))
  soma = soma + termo
  ex = ex + 1
 end
return soma
end

#Seno 
function taylor_sin(x)
Erro = 1e-7
termo = 1
soma = x
ex = 1
 while abs(termo) > Erro
  termo = (potencia(-1,ex) * potencia(x,2*ex+1)) / fatorial(2*ex+1)
  soma = soma + termo
  ex = ex + 1
 end
return soma
end

#Tangente
function taylor_tan(x)
Erro = 0.001
termo = 1
soma = x
ex = 2
 while termo > Erro
  numerador = (potencia(2,2*ex) * (potencia(2,2*ex) - 1) * potencia(x,2*ex-1))
  denominador = fatorial(2*ex)
  termo = numerador * bernoulli(ex) / denominador
  soma = soma + termo
  ex = ex + 1
 end
return convert(Float64, big(soma))
end
=======
#Teste Geral
function test()
check_cos(cos(pi/3),taylor_cos(pi/3))
check_cos(cos(pi/4),taylor_cos(pi/4))
check_cos(cos(pi/6),taylor_cos(pi/6))
check_cos(cos(2pi/3),taylor_cos(2pi/3))
check_cos(cos(3pi/4),taylor_cos(3pi/4))
check_cos(cos(5pi/6),taylor_cos(5pi/6))
check_sin(sin(pi/2),taylor_sin(pi/2))                          
check_sin(sin(pi/3),taylor_sin(pi/3))
check_sin(sin(pi/4),taylor_sin(pi/4))
check_sin(sin(pi/6),taylor_sin(pi/6))
check_sin(sin(2pi/3),taylor_sin(2pi/3))
check_sin(sin(3pi/4),taylor_sin(3pi/4))
check_sin(sin(5pi/6),taylor_sin(5pi/6))
check_tan(tan(pi/3),taylor_tan(pi/3))
check_tan(tan(pi/4),taylor_tan(pi/4))
check_tan(tan(pi/6),taylor_tan(pi/6))
>>>>>>> parte3
end

